<?php
namespace Swissclinic\Flowbox\Block;

/**
 * Swiss Clinic Flowbox Block
 * @api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Flowbox extends \Magento\Framework\View\Element\Template {

    /**
     * @var Swissclinic\Flowbox\Helper\Data
     */
    protected $_shlp;

    protected $_store;

    protected $_coreRegistry = null;

    /**
     * @var Product
     */
    protected $_product = null;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = [],
        \Swissclinic\Flowbox\Helper\Data $shlp,
        \Magento\Framework\Registry $registry
    ) {

        $this->_shlp = $shlp;

        $this->_store = $storeManager->getStore();

        $this->_coreRegistry = $registry;

        parent::__construct(
            $context,
            $data
        );
    }

    public function getFlowbox() {

        $storeId = $this->_store->getId();
        $id = null;

        $id = $this->_shlp->getFeedId($storeId);

        return $id;
    }

    public function getProductFeedId() {

        $storeId = $this->_store->getId();
        $id = null;

        $id = $this->_shlp->getProductFeedId($storeId);

        return $id;
    }

    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    public function isProductFeedsEnabled()
    {
        return $this->_shlp->isProductFeedsEnabled();
    }
}