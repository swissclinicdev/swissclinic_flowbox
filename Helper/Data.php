<?php

namespace Swissclinic\Flowbox\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const XML_PATH_SWISSCLINIC_FLOWBOX_CONFIG = 'swissclinic_flowbox/flowbox_config/enable';
    const XML_PATH_SWISSCLINIC_FLOWBOX_FEED_ID = 'swissclinic_flowbox/flowbox_config/feed_id';
    const XML_PATH_SWISSCLINIC_FLOWBOX_PRODUCT_FEED_ID = 'swissclinic_flowbox/flowbox_config/product_feed_id';
    const XML_PATH_SWISSCLINIC_FLOWBOX_PRODUCTFEEDS = 'swissclinic_flowbox/flowbox_config/product_feeds_enabled';


    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    )
    {
        parent::__construct($context);
    }


    /**
     * @param null $storeId
     * @return string
     */
    public function getFeedId($storeId = null)
    {
        $id = $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_FLOWBOX_FEED_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $id;
    }

    /**
     * @param null $storeId
     * @return string
     */
    public function getProductFeedId($storeId = null)
    {
        $id = $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_FLOWBOX_PRODUCT_FEED_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $id;
    }

    public function isProductFeedsEnabled($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_FLOWBOX_PRODUCTFEEDS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}